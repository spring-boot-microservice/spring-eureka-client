package com.rinkesh.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author RinkeshKM
 * @project spring-eureka-client
 * @created 17/03/2021 - 12:40 PM
 */

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @GetMapping("/list")
    public Map<String, String> getEmpList(){
        Map<String, String> empMap = new HashMap<>();

        empMap.put("emp1", "rinkesh");
        empMap.put("emp3", "aniket");

        return empMap;
    }
}
